package dao.implementations;

import dao.conn.ConnectionFactory;
import dao.interfaces.DAO;
import model.Request;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RequestDao implements DAO<Request>
{
    private static final String CREATE = "create table if not exists renel.request" +
            "(id int not null auto_increment unique," +
            " accepted tinyint(1) not null," +
            " clientId int not null," +
            " description varchar(1024) not null," +
            " primary key (id))";

    private static final String INSERT = "insert into request (accepted, clientId, description) values (?, ?, ?)";
    private static final String UPDATE = "update complaint set accepted = ? where id = ?";
    private static final String SELECT_BY_ID = "select * from request where id = ?";
    private static final String SELECT_ALL = "select * from request";

    private final Connection connection;

    public RequestDao()
    {
        Statement statement;

        connection = ConnectionFactory.getConnection();
        try
        {
            statement = connection.createStatement();

            statement.execute(CREATE);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void insert(Request request) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(INSERT);

        statement.setInt(1, request.getClientId());
        statement.setString(2, request.getDescription());

        statement.executeUpdate();
    }

    @Override
    public void update(Request request) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE);

        statement.setBoolean(1, request.isAccepted());
        statement.setInt(2, request.getId());

        statement.executeUpdate();
    }

    @Deprecated
    @Override
    public void delete(Request request) throws SQLException {
        // LOL
    }

    @Override
    public Request select(int id) throws SQLException {
        Request request = null;
        ResultSet result;

        PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID);

        statement.setInt(1, id);

        result = statement.executeQuery();

        if (result.next())
        {
            request = new Request(
                    result.getInt("id"),
                    result.getBoolean("accepted"),
                    result.getInt("clientId"),
                    result.getString("description")
            );
        }
        return request;
    }

    @Deprecated
    @Override
    public Request select(String username) throws SQLException {
        // LOL
        return null;
    }

    @Override
    public List<Request> selectAll() throws SQLException {
        List<Request> list = new ArrayList<>();
        ResultSet result;
        PreparedStatement statement = connection.prepareStatement(SELECT_ALL);

        result = statement.executeQuery();

        while(result.next())
        {
            list.add(new Request(
                    result.getInt("id"),
                    result.getBoolean("accepted"),
                    result.getInt("clientId"),
                    result.getString("description")
            ));
        }

        return list;
    }
}
