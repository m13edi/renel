package dao.implementations;

import dao.conn.ConnectionFactory;
import dao.interfaces.DAO;
import model.Client;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ClientDao implements DAO<Client>
{
    // db related
    private static final String CREATE = "create table if not exists renel.client" +
            " (id int not null auto_increment unique," +
            " username varchar(25) not null unique," +
            " password varchar(25) not null," +
            " name varchar(45) not null," +
            " address varchar(45) not null," +
            " idn int not null," +
            " primary key (id))";

    // query
    private static final String INSERT = "insert into client (username, password, name, address, idn) values(?, ?, ?, ?, ?)";
    private static final String DELETE = "delete from client where id = ?";
    private static final String UPDATE = "update client set username = ?, password = ?, name = ?, address = ?, idn = ? where id = ?";
    private static final String SELECT_ALL = "select * from client";
    private static final String SELECT_BY_ID = "select * from client where id = ?";
    private static final String SELECT_BY_USER = "select * from client where username = ?";

    private final Connection connection;

    public ClientDao()
    {
        Statement statement;

        connection = ConnectionFactory.getConnection();
        try
        {
            statement = connection.createStatement();

            statement.execute(CREATE);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void insert(Client client) throws SQLException
    {
        PreparedStatement statement = connection.prepareStatement(INSERT);

        statement.setString(1, client.getUsername());
        statement.setString(2, client.getPassword());
        statement.setString(3, client.getName());
        statement.setString(4, client.getAddress());
        statement.setInt(5, client.getIdc());

        statement.executeUpdate();
    }

    @Override
    public void update(Client client) throws SQLException
    {
        PreparedStatement statement = connection.prepareStatement(UPDATE);

        statement.setString(1, client.getUsername());
        statement.setString(2, client.getPassword());
        statement.setString(3, client.getName());
        statement.setString(4, client.getAddress());
        statement.setInt(5, client.getIdc());
        statement.setInt(6, client.getId());

        statement.executeUpdate();
    }

    @Override
    public void delete(Client client) throws SQLException
    {
        PreparedStatement statement = connection.prepareStatement(DELETE);

        statement.setInt(1, client.getId());

        statement.executeUpdate();
    }

    @Override
    public Client select(int id) throws SQLException{
        Client client = null;
        ResultSet result;

        PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID);

        statement.setInt(1, id);

        result = statement.executeQuery();

        if (result.next())
        {
            client = new Client(
                    result.getInt("id"),
                    result.getString("username"),
                    result.getString("password"),
                    result.getString("name"),
                    result.getString("address"),
                    result.getInt("idn")
            );
        }
        return client;
    }

    @Override
    public Client select(String username) throws SQLException {
        Client client = null;
        ResultSet result;

        PreparedStatement statement = connection.prepareStatement(SELECT_BY_USER);

        statement.setString(1, username);

        result = statement.executeQuery();

        if (result.next())
        {
            client = new Client(
                    result.getInt("id"),
                    result.getString("username"),
                    result.getString("password"),
                    result.getString("name"),
                    result.getString("address"),
                    result.getInt("idn")
            );
        }
        return client;
    }

    @Override
    public List<Client> selectAll() throws SQLException
    {
        List<Client> list = new ArrayList<>();
        ResultSet result;
        PreparedStatement statement = connection.prepareStatement(SELECT_ALL);

        result = statement.executeQuery();

        while(result.next())
        {
            list.add(new Client(
                    result.getInt("id"),
                    result.getString("username"),
                    result.getString("password"),
                    result.getString("name"),
                    result.getString("address"),
                    result.getInt("idn")
            ));
        }

        return list;
    }

    public static void main(String argv[])
    {
        ClientDao dao = new ClientDao();
        Client client1 = new Client(0,"ceausescu", "elenaprintesa", "Nicolae Ceausescu", "Palatul Cotroceni", 0);
        Client client2 = new Client(0, "sefuPapadiilor", "regeleManelelor", "Florin Salam", "Taraf TV si FM", 1);
        List<Client> list;

        try
        {
            dao.insert(client1);
            dao.insert(client2);

            list = dao.selectAll();
            System.out.println(list);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
}
