package dao.implementations;

import dao.conn.ConnectionFactory;
import dao.interfaces.DAO;
import model.Complaint;

import java.sql.*;
import java.util.List;

public class ComplaintDao implements DAO<Complaint>
{
    private static final String CREATE = "create table if not exists renel.complaint" +
            "(id int not null auto_increment unique," +
            " requestId int not null," +
            " description varchar(1024) not null," +
            " date date not null," +
            " quality int not null," +
            " response int not null," +
            " service int not null," +
            " primary key (id))";

    private static final String INSERT = "insert into complaint (requestId, description, date, quality, response, service) values (?, ?, ?, ?, ?, ?)";
    private static final String DELETE = "delete from complaint where id = ?";
    private static final String UPDATE = "update complaint set description = ?, date = ?, quality = ?, response = ?, service = ? where id = ?";
    private static final String SELECT_ALL = "select * from complaint";
    private static final String SELECT_BY_ID = "select * from complaint where id = ?";

    private final Connection connection;

    public ComplaintDao()
    {
        Statement statement;

        connection = ConnectionFactory.getConnection();
        try
        {
            statement = connection.createStatement();

            statement.execute(CREATE);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void insert(Complaint complaint) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(INSERT);

        statement.setString(1, complaint.getDescription());
        statement.setDate(2, complaint.getDate());
        statement.setInt(3, complaint.getQuality());
        statement.setInt(4, complaint.getResponse());
        statement.setInt(5, complaint.getService());

        statement.executeUpdate();
    }

    @Override
    public void update(Complaint complaint) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE);

        statement.setString(1, complaint.getDescription());
        statement.setDate(2, complaint.getDate());
        statement.setInt(3, complaint.getQuality());
        statement.setInt(4, complaint.getResponse());
        statement.setInt(5, complaint.getService());
        statement.setInt(6, complaint.getId());

        statement.executeUpdate();
    }

    @Override
    public void delete(Complaint complaint) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(DELETE);

        statement.setInt(1, complaint.getId());

        statement.executeUpdate();
    }

    @Override
    public Complaint select(int id) throws SQLException {
        Complaint complaint = null;
        ResultSet result;

        PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID);

        statement.setInt(1, id);

        result = statement.executeQuery();

        if (result.next())
        {
            complaint = new Complaint(
                    result.getInt("id"),
                    result.getInt("requestId"),
                    result.getString("description"),
                    result.getDate("date"),
                    result.getInt("quality"),
                    result.getInt("response"),
                    result.getInt("service")
            );
        }
        return complaint;
    }

    @Deprecated
    @Override
    public Complaint select(String username) throws SQLException {
        // LOL
        return null;
    }

    @Override
    public List<Complaint> selectAll() throws SQLException {
        return null;
    }
}
