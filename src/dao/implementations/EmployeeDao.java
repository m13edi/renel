package dao.implementations;

import dao.conn.ConnectionFactory;
import dao.interfaces.DAO;
import model.AbstractEmployee;
import model.Employee;
import model.Manager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDao implements DAO<AbstractEmployee> {

    private static final String CREATE = "create table if not exists renel.employee" +
            " (id int not null auto_increment unique," +
            " username varchar(25) not null unique," +
            " password varchar(25) not null," +
            " isManager tinyint(1) not null," +
            " primary key (id))";

    // query
    private static final String INSERT = "insert into employee (username, password, isManager) values(?, ?, ?)";
    private static final String DELETE = "delete from employee where id = ?";
    private static final String SELECT_ALL = "select * from employee";
    private static final String SELECT_BY_ID = "select * from employee where id = ?";
    private static final String SELECT_BY_USER = "select * from employee where username = ?";

    private final Connection connection;

    public EmployeeDao()
    {
        Statement statement;

        connection = ConnectionFactory.getConnection();
        try
        {
            statement = connection.createStatement();

            statement.execute(CREATE);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void insert(AbstractEmployee employee) throws SQLException
    {
        PreparedStatement statement = connection.prepareStatement(INSERT);
        boolean isManager;

        if (employee.getClass().getSimpleName().equals(Manager.class.getSimpleName()))
        {
            isManager = true;
        }
        else
        {
            isManager = false;
        }

        statement.setString(1, employee.getUsername());
        statement.setString(2, employee.getPassword());
        statement.setBoolean(3, isManager);

        statement.executeUpdate();
    }

    @Override
    @Deprecated
    public void update(AbstractEmployee employee) throws SQLException {
        // LOL
    }

    @Override
    public void delete(AbstractEmployee employee) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(DELETE);

        statement.setInt(1, employee.getId());

        statement.executeUpdate();
    }

    @Override
    public AbstractEmployee select(int id) throws SQLException {
        AbstractEmployee employee = null;
        ResultSet result;

        PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID);

        statement.setInt(1, id);

        result = statement.executeQuery();

        if (result.next())
        {
            if (result.getBoolean("isManager"))
            {
                employee = new Manager(
                        result.getInt("id"),
                        result.getString("username"),
                        result.getString("password")
                );
            }
            else
            {
                employee = new Employee(
                        result.getInt("id"),
                        result.getString("username"),
                        result.getString("password")
                );
            }
        }
        return employee;
    }

    @Override
    public AbstractEmployee select(String username) throws SQLException {
        AbstractEmployee employee = null;
        ResultSet result;

        PreparedStatement statement = connection.prepareStatement(SELECT_BY_USER);

        statement.setString(1, username);

        result = statement.executeQuery();

        if (result.next())
        {
            if (result.getBoolean("isManager"))
            {
                employee = new Manager(
                        result.getInt("id"),
                        result.getString("username"),
                        result.getString("password")
                );
            }
            else
            {
                employee = new Employee(
                        result.getInt("id"),
                        result.getString("username"),
                        result.getString("password")
                );
            }
        }
        return employee;
    }

    @Override
    public List<AbstractEmployee> selectAll() throws SQLException {
        List<AbstractEmployee> list = new ArrayList<>();
        ResultSet result;
        PreparedStatement statement = connection.prepareStatement(SELECT_ALL);

        result = statement.executeQuery();

        while (result.next())
        {
            if (result.getBoolean("isManager"))
            {
                list.add( new Manager(
                        result.getInt("id"),
                        result.getString("username"),
                        result.getString("password")
                ));
            }
            else
            {
                list.add( new Employee(
                        result.getInt("id"),
                        result.getString("username"),
                        result.getString("password")
                ));
            }
        }


        return list;
    }

    public static void main(String argv[])
    {
        Manager m = new Manager(1, "stefanMarele", "vin ieftin");
        Employee e = new Employee(2, "cuzavoda", "cuza v-o da");
        List<AbstractEmployee> list;

        EmployeeDao dao = new EmployeeDao();

        try
        {
            dao.insert(m);
            dao.insert(e);
            list = dao.selectAll();
            System.out.println(list);
        }
        catch (SQLException ex)
        {
            ex.printStackTrace();
        }
    }
}
