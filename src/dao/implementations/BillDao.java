package dao.implementations;

import dao.conn.ConnectionFactory;
import dao.interfaces.DAO;
import model.Bill;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BillDao implements DAO<Bill>
{
    private static final String CREATE = "create table if not exists renel.bill" +
            "(id int not null auto_increment unique," +
            " clientId int not null," +
            " balance float not null," +
            " date Date not null," +
            " primary key (id))";

    // query
    private static final String INSERT = "insert into bill (clientId, balance, date) values(?, ?, ?)";
    private static final String DELETE = "delete from bill where id = ?";
    private static final String UPDATE = "update bill set balance = ? where id = ?";
    private static final String SELECT_ALL = "select * from bill";
    private static final String SELECT_BY_ID = "select * from bill where id = ?";
    private static final String SELECT_BY_USER = "select * from bill where clientId = ?";

    private final Connection connection;

    public BillDao()
    {
        Statement statement;

        connection = ConnectionFactory.getConnection();
        try
        {
            statement = connection.createStatement();

            statement.execute(CREATE);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void insert(Bill bill) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(INSERT);

        statement.setInt(1, bill.getClientId());
        statement.setFloat(2, bill.getBalance());
        statement.setDate(3, bill.getDate());

        statement.executeUpdate();
    }

    @Override
    public void update(Bill bill) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE);

        statement.setFloat(1, bill.getBalance());
        statement.setInt(2, bill.getId());

        statement.executeUpdate();
    }

    @Override
    @Deprecated
    public void delete(Bill bill) throws SQLException {
        // LOL
    }

    @Override
    public Bill select(int id) throws SQLException {
        Bill bill = null;
        ResultSet result;

        PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID);

        statement.setInt(1, id);

        result = statement.executeQuery();

        if (result.next())
        {
            bill = new Bill(
                    result.getInt("id"),
                    result.getInt("clientId"),
                    result.getFloat("balance"),
                    result.getDate("date")
            );
        }
        return bill;
    }

    @Override
    @Deprecated
    public Bill select(String username) throws SQLException {
        // LOL
        return null;
    }

    @Override
    @Deprecated
    public List<Bill> selectAll() throws SQLException {
        return null;
    }

    public List<Bill> selectAll(int clientId) throws SQLException {
        List<Bill> list = new ArrayList<>();
        ResultSet result;
        PreparedStatement statement = connection.prepareStatement(SELECT_BY_USER);

        statement.setInt(1, clientId);

        result = statement.executeQuery();

        while(result.next())
        {
            list.add(new Bill(
                    result.getInt("id"),
                    result.getInt("clientId"),
                    result.getFloat("balance"),
                    result.getDate("date")
            ));
        }

        return list;
    }
}
