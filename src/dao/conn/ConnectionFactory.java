package dao.conn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

public class ConnectionFactory
{
    // private static final Logger LOGGER = Logger.getLogger(ConnectionSingleton.class.getName());
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/?useSSL=false";
    private static final String USER = "root";
    private static final String PASS = "root";

    private static final String CREATE = "create schema if not exists renel";
    private static final String USE = "use renel";

    private static ConnectionFactory instance = null;

    private ConnectionFactory()
    {
        try
        {
            Class.forName(DRIVER);
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    private Connection createConnection()
    {
        Connection connection = null;
        try
        {
            connection = DriverManager.getConnection(DBURL, USER, PASS);
        }
        catch (SQLException e)
        {
            // LOGGER.log()
            e.printStackTrace();
        }
        return connection;
    }

    public static Connection getConnection()
    {
        Connection connection = null;
        Statement statement;

        if (null == instance)
        {
            instance = new ConnectionFactory();
        }

        try
        {
            connection = instance.createConnection();
            statement = connection.createStatement();

            statement.execute(CREATE);
            statement.execute(USE);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return connection;
    }

    public void close(Connection connection)
    {
        try
        {
            connection.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
}
