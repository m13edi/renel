package dao.interfaces;

import model.Client;

import java.sql.SQLException;
import java.util.List;

public interface DAO<E>
{
    void insert(E e) throws SQLException;

    void update(E e) throws SQLException;

    void delete(E e) throws SQLException;

    E select(int id) throws SQLException;

    E select(String username) throws SQLException;

    List<E> selectAll() throws SQLException;
}
