package model;

public class Manager extends AbstractEmployee
{
    public Manager(int id, String username, String password)
    {
        super(username, password, id);
    }

    @Override
    public String toString()
    {
        return "Manager " + super.toString();
    }
}
