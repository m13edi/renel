package model;

public class Employee extends AbstractEmployee
{
    public Employee(int id, String username, String password)
    {
        super(username, password, id);
    }

    @Override
    public String toString()
    {
        return "Employee " + super.toString();
    }
}
