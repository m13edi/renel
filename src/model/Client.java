package model;

public class Client extends User
{
    private String name;
    private String address;
    private int idc;

    public Client(int id, String username, String password, String name, String address, int idc)
    {
        super(username, password, id);

        this.name = name;
        this.address = address;
        this.idc = idc;
    }

    public String getName()
    {
        return name;
    }

    public String getAddress()
    {
        return address;
    }

    public int getIdc()
    {
        return idc;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public void setIdc(int idc)
    {
        this.idc = idc;
    }

    @Override
    public String toString()
    {
        return super.toString() + " name: " + name + " address: " + address + " idc: " + idc;
    }
}
