package model;

import java.sql.Date;

public class Complaint
{
    private int id;
    private int requestId;
    private String description;
    private Date date;
    private int quality;
    private int response;
    private int service;

    public Complaint(int id, int requestId, String description, Date date, int quality, int response, int service)
    {
        this.id = id;
        this.requestId = requestId;
        this.description = description;
        this.date = date;
        this.quality = quality;
        this.response = response;
        this.service = service;
    }

    public int getId()
    {
        return id;
    }

    public int getRequestId()
    {
        return requestId;
    }

    public String getDescription()
    {
        return description;
    }

    public Date getDate()
    {
        return date;
    }

    public int getQuality()
    {
        return quality;
    }

    public int getResponse()
    {
        return response;
    }

    public int getService()
    {
        return service;
    }


}
