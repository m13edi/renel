package model;

import java.sql.Date;

public class Bill
{
    private int id;
    private int clientId;
    private float balance;
    private Date date;

    public Bill(int id, int clientId, float balance, Date date)
    {
        this.id = id;
        this.clientId = clientId;
        this.balance = balance;
        this.date = date;
    }

    public int getId()
    {
        return id;
    }

    public int getClientId()
    {
        return clientId;
    }

    public float getBalance()
    {
        return balance;
    }

    public Date getDate()
    {
        return date;
    }

    public void setBalance(float balance)
    {
        this.balance -= balance;
    }

    @Override
    public String toString()
    {
        return "Bill id: " + id + " clientId: " + clientId + " ballance: " + balance + " date: " + date;
    }
}
