package model;

public class Request
{
    private int id;
    private int clientId;
    public boolean accepted;
    private String description;

    public Request(int id, boolean accepted, int clientId, String description)
    {
        this.id = id;
        this.accepted = accepted;
        this.clientId = clientId;
        this.description = description;
    }

    public int getId()
    {
        return id;
    }

    public boolean isAccepted()
    {
        return accepted;
    }

    public void setAccepted(boolean accepted)
    {
        this.accepted = accepted;
    }

    public int getClientId()
    {
        return clientId;
    }

    public String getDescription()
    {
        return description;
    }
}
