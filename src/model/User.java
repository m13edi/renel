package model;

import java.security.InvalidParameterException;

public abstract class User
{
    private String username;
    private String password;
    private int id;

    public User(String username, String password, int id) throws InvalidParameterException
    {
        if (null == password || password.length() < 8)
        {
            throw new InvalidParameterException("Password must be at least 8 chars long!");
        }
        if (null == username || username.length() < 8)
        {
            throw new InvalidParameterException("Username must be at least 8 chars long!");
        }

        this.username = username;
        this.password = password;
        this.id = id;
    }

    public void setPassword(String password) throws InvalidParameterException
    {
        if (password.length() < 8)
        {
            throw new InvalidParameterException("Password must be at least 8 chars long!");
        }
        this.password = password;
    }

    public void setUsername(String username) throws InvalidParameterException
    {
        if (username.length() < 8)
        {
            throw new InvalidParameterException("Username must be at least 8 chars long!");
        }
        this.username = username;
    }

    public String getUsername()
    {
        return username;
    }

    public String getPassword()
    {
        // Because encryption is overrated.
        return password;
    }

    public int getId()
    {
        return id;
    }

    @Override
    public String toString()
    {
        return "Id: " + id + " User: " + username + " Pass: " + password;
    }
}
