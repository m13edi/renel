package view;

import blogic.ClientBl;
import blogic.EmployeeBl;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginFrame
{
    private JTextField usernameText;
    private JPasswordField passwordField;
    private JButton signUpButton;
    private JButton logInButton;
    private JPanel mainPanel;
    private JCheckBox employeeCheckBox;
    private JLabel usernameLabel;
    private JLabel passwordLabel;

    public LoginFrame() {
        signUpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // goto sign up frame
                // close this frame
            }
        });

        logInButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ClientBl clientBl = ClientBl.getInstance();
                EmployeeBl employeeBl = EmployeeBl.getInstance();
                String username = usernameText.getText();
                String password = new String(passwordField.getPassword());

                if (employeeCheckBox.isSelected())
                {
                    if (employeeBl.authenticate(username, password))
                    {
                        System.out.println("Employee connected!");
                        // close frame
                        // go to employee frame
                    }
                    else
                    {
                        usernameLabel.setText("Username Invalid username or password!");
                    }
                }
                else
                {
                    if (clientBl.authenticate(username, password))
                    {
                        System.out.println("Employee connected!");
                        // close frame
                        // go to employee frame
                    }
                    else
                    {
                        usernameLabel.setText("Username Invalid username or password!");
                    }
                }
            }
        });
    }

    public static void main(String argv[])
    {
        JFrame frame = new JFrame("Login");
        frame.setContentPane(new LoginFrame().mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
