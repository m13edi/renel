package blogic;

import dao.implementations.BillDao;
import dao.implementations.ClientDao;
import dao.implementations.ComplaintDao;
import dao.implementations.RequestDao;
import model.Bill;
import model.Client;
import model.Complaint;
import model.Request;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClientBl
{
    private ClientDao clientDao;
    private BillDao billDao;
    private RequestDao requestDao;
    private ComplaintDao complaintDao;

    private static ClientBl instance = null;

    private ClientBl()
    {
        clientDao = new ClientDao();
        billDao = new BillDao();
        requestDao = new RequestDao();
        complaintDao = new ComplaintDao();
    }

    public static ClientBl getInstance()
    {
        if (null == instance)
        {
            instance = new ClientBl();
        }
        return instance;
    }

    public boolean requestNewAccount(Client newClient)
    {
        try
        {
            Client client = clientDao.select(newClient.getId());

            if (null == client)
            {
                return false;
            }

            client.setUsername(newClient.getUsername());
            client.setPassword(newClient.getPassword());
            client.setName(newClient.getName());
            client.setAddress(newClient.getAddress());
            client.setIdc(newClient.getIdc());

            clientDao.update(client);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean authenticate(String username, String password)
    {
        try
        {
            Client client = clientDao.select(username);

            if (null == client || !client.getPassword().equals(password))
            {
                return false;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public List<Bill> getBills(Client client)
    {
        List<Bill> list = null;
        List<Bill> unpaidBills = new ArrayList<>();
        try
        {
            list = billDao.selectAll(client.getId());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }

        for (Bill bill : list)
        {
            if (bill.getBalance() > 0)
            {
                unpaidBills.add(bill);
            }
        }

        return unpaidBills;
    }

    public boolean payBill(Bill bill, float amount)
    {
        try
        {
            Bill newBill = billDao.select(bill.getId());
            if (null == newBill)
            {
                return false;
            }
            newBill.setBalance(amount);

            billDao.update(newBill);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public List<Bill> getAllBills(Client client)
    {
        List<Bill> list = null;

        try
        {
            list = billDao.selectAll(client.getId());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }

        return list;
    }

    public boolean requestComplaint(Request request)
    {
        try
        {
            requestDao.insert(request);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public List<Complaint> getComplaints(Client client)
    {
        List<Complaint> list = null;
        List<Complaint> clientList = new ArrayList<>();
        List<Request> requests = null;

        try
        {
            list = complaintDao.selectAll();
            requests = requestDao.selectAll();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }

        // SQL Queries are overrated :)
        for (Request r : requests)
        {
            if (r.getClientId() == client.getId())
            {
                for (Complaint c : list)
                {
                    if (c.getRequestId() == r.getId())
                    {
                        clientList.add(c);
                    }
                }
            }
        }
        return clientList;
    }

    public boolean completeForm(Complaint complaint)
    {
        Request request;

        try
        {
            request = requestDao.select(complaint.getRequestId());

            if (null == request || !request.isAccepted())
            {
                return false;
            }

            complaintDao.update(complaint);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
