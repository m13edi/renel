package blogic;

import dao.implementations.ClientDao;
import dao.implementations.ComplaintDao;
import dao.implementations.EmployeeDao;
import model.AbstractEmployee;
import model.Client;
import model.Complaint;

import java.sql.SQLException;
import java.util.List;

public class ManagerBl
{
    private ComplaintDao complaintDao;
    private ClientDao clientDao;
    private EmployeeDao employeeDao;

    private ManagerBl instance = null;

    private ManagerBl()
    {
        complaintDao = new ComplaintDao();
        clientDao = new ClientDao();
        employeeDao = new EmployeeDao();
    }

    public ManagerBl getInstance()
    {
        if (null == instance)
        {
            instance = new ManagerBl();
        }
        return instance;
    }

    public boolean authenticate(String username, String password)
    {
        try
        {
            AbstractEmployee employee = employeeDao.select(username);

            if (null == employee || !employee.getPassword().equals(password))
            {
                return false;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean createClientId(int id)
    {
        Client client = new Client(id, null, null, null, null, 0);
        try
        {
            clientDao.insert(client);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean createClient(Client client)
    {
        try
        {
            clientDao.insert(client);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public List<Client> readClients()
    {
        List<Client> list = null;
        try
        {
            list = clientDao.selectAll();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
        return list;
    }

    public boolean updateClient(Client client)
    {
        try
        {
            clientDao.update(client);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean deleteClient(Client client)
    {
        try
        {
            clientDao.delete(client);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean createComplaint(Complaint complaint)
    {
        try
        {
            complaintDao.insert(complaint);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public List<Complaint> readComplaints()
    {
        List<Complaint> list = null;
        try
        {
            list = complaintDao.selectAll();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
        return list;
    }

    public boolean updateComplaint(Complaint complaint)
    {
        try
        {
            complaintDao.update(complaint);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean deleteComplaint(Complaint complaint)
    {
        try
        {
            complaintDao.delete(complaint);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
