package blogic;

import dao.implementations.ClientDao;
import dao.implementations.ComplaintDao;
import dao.implementations.EmployeeDao;
import dao.implementations.RequestDao;
import model.AbstractEmployee;
import model.Client;
import model.Complaint;
import model.Request;

import java.sql.Date;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

public class EmployeeBl
{
    private ClientDao clientDao;
    private RequestDao requestDao;
    private ComplaintDao complaintDao;
    private EmployeeDao employeeDao;

    private static EmployeeBl instance = null;

    private EmployeeBl()
    {
        clientDao = new ClientDao();
        requestDao = new RequestDao();
        complaintDao = new ComplaintDao();
        employeeDao = new EmployeeDao();
    }

    public static EmployeeBl getInstance()
    {
        if (null == instance)
        {
            instance = new EmployeeBl();
        }
        return instance;
    }

    public boolean authenticate(String username, String password)
    {
        try
        {
            AbstractEmployee employee = employeeDao.select(username);

            if (null == employee || !employee.getPassword().equals(password))
            {
                return false;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public List<Client> viewClients()
    {
        List<Client> clients = null;

        try
        {
            clients = clientDao.selectAll();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
        return clients;
    }

    public boolean update(Client client)
    {
        try
        {
            clientDao.update(client);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean acceptRequest(Request request)
    {
        request.setAccepted(true);
        Complaint complaint;
        Calendar cal = Calendar.getInstance();

        try
        {
            complaint = new Complaint(0, request.getId(), null, new Date(cal.getTime().getTime()), 0, 0, 0);

            requestDao.update(request);
            complaintDao.insert(complaint);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
